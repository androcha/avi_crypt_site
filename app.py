from flask import Flask, request, Response, render_template, redirect, jsonify
from math import floor
from sqlite3 import OperationalError
import string, sqlite3, base64, json, ast

#Assuming urls.db is in your app root folder
app = Flask(__name__)
app_name = 'url-short'
from urlparse import urlparse
host = 'http://localhost:5000/'

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        return Response('OK')
    if request.method == 'POST':
        pass
    return Response('OK')


if __name__ == '__main__':
    # This code checks whether database table is created or not
    app.run(debug=True)